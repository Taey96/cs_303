/* Traffic Light Controller
 *
 * --- Code is best viewed with the tab size of 4. ---
 */

#include <system.h>
#include <sys/alt_alarm.h>
#include <sys/alt_irq.h>
#include <altera_avalon_pio_regs.h>
#include <alt_types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define CLEAR_LCD_STRING "[2J"
#define ESC 27
// A template for COMPSYS 303 Assignment 1
//
// NOTE: You do not need to use this! Feel free
// to write your own code from scratch if you
// want, this is purely an example

// FUNCTION PROTOTYPES
// Timer ISRs
alt_u32 tlc_timer_isr();
alt_u32 camera_timer_isr(void* context);

//  Misc
// Others maybe added eg LEDs / UART
void lcd_set_mode(unsigned int mode);

// TLC state machine functions
void init_tlc(void);
void simple_tlc(int* state);
void pedestrian_tlc(int* state);
void configurable_tlc(int* state);
int config_tlc(int *tl_state);
void camera_tlc(int* state);

// Button Inputs / Interrupts
void buttons_driver(int* button);
void handle_mode_button(unsigned int* taskid);
void handle_vehicle_button(void);
void init_buttons_pio(int buttons);
void NSEW_ped_isr(void* context, alt_u32 id);

// Red light Camera
void clear_vehicle_detected(void);
void vehicle_checked(void);
int is_vehicle_detected(void);
int is_vehicle_left(void);

// Configuration Functions
int update_timeout(void);
void config_isr(void* context, alt_u32 id);
void buffer_timeout(unsigned int value);
void timeout_data_handler(void);
// CONSTANTS
#define OPERATION_MODES 0x03	// number of operation modes (00 - 03 = 4 modes)
#define CAMERA_TIMEOUT	2000	// timeout period of red light camera (in ms)
#define TIMEOUT_NUM 6			// number of timeouts
#define TIME_LEN 8				// buffer length for time digits


// USER DATA TYPES
// Timeout buffer structure
typedef struct  {
	int index;
	unsigned int timeout[TIMEOUT_NUM];
} TimeBuf;


// GLOBAL VARIABLES
static alt_alarm tlc_timer;		// alarm used for traffic light timing
static alt_alarm camera_timer;	// alarm used for camera timing

// NOTE:
// set contexts for ISRs to be volatile to avoid unwanted Compiler optimisation
static volatile int tlc_timer_event = 0;
static volatile int camera_timer_event = 0;
static volatile int pedestrianNS = 0;
static volatile int pedestrianEW = 0;

// 4 States of 'Detection':
// Car Absent
// Car Enters
// Car is Detected running a Red
// Car Leaves
static int vehicle_detected = 0;
static int error = 0;
static int endString = 0;

// Traffic light timeouts
static unsigned int timeout[TIMEOUT_NUM] = {1000, 1000, 1000, 1000, 1000, 1000};
//static unsigned int timeout[TIMEOUT_NUM] = {4000, 4000, 4000, 4000, 4000, 4000};
static TimeBuf timeout_buf = { -1, {500, 6000, 2000, 500, 6000, 2000} };
static int timearray[] = {0, 0, 0, 0, 0, 0};
static int arrayCount = 0;
// UART
FILE* uart;
// LCD display
FILE* lcd;

// Traffic light LED values
//static unsigned char traffic_lights[TIMEOUT_NUM] = {0x90, 0x50, 0x30, 0x90, 0x88, 0x84};
// NS RGY | EW RGY
// NR,NG | NY,ER,EG,EY
static unsigned char traffic_lights[TIMEOUT_NUM] = {0x24, 0x14, 0x0C, 0x24, 0x22, 0x21};
enum traffic_states {RR0, GR, YR, RR1, RG, RY};
static enum traffic_states currentState = RR0;

static unsigned int configSwitch = 0;
static unsigned int mode = 0;
static unsigned int previousMode = 0;
static unsigned int timerFlag = 0;
// Process states: use -1 as initialization state
static int proc_state[OPERATION_MODES + 1] = {-1, -1, -1, -1};

// Initialize the traffic light controller
// for any / all modes
void init_tlc(void)
{
	if (mode == 1 || mode == 2 || mode == 4) {
		currentState = RR0;
	}
}


/* DESCRIPTION: Writes the mode to the LCD screen
 * PARAMETER:   mode - the current mode
 * RETURNS:     none
 */
void lcd_set_mode(unsigned int mode)
{
	int outputMode = 0;
	switch (mode) {
		case 1:
			outputMode = 1;
			break;
		case 2:
			outputMode = 2;
			break;
		case 4:
			outputMode = 3;
			break;
		case 8:
			outputMode = 4;
			break;
		case 131076:
			outputMode = 5;
			break;
		default:
			outputMode = 420;		// trolololol
			break;
	}

	fprintf(lcd, "%c%s", ESC, CLEAR_LCD_STRING);
	fprintf(lcd, "Mode: %d\n", outputMode);
	previousMode = mode;
}

/* DESCRIPTION: Performs button-press detection and debouncing
 * PARAMETER:   button - referenced argument to indicate the state of the button
 * RETURNS:     none
 */
void buttons_driver(int* button)
{
	// Persistant state of 'buttons_driver'
	static int state = 0;

	*button = 0;	// no assumption is made on intial value of *button
	// Debounce state machine
		// call handle_mode_button()
}


/* DESCRIPTION: Updates the ID of the task to be executed and the 7-segment display
 * PARAMETER:   taskid - current task ID
 * RETURNS:     none
 */
void handle_mode_button(unsigned int* taskid)
{
	// Increment mode
	// Update Mode-display
}


/* DESCRIPTION: Simple traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
void simple_tlc(int* state)
{
	if (mode == 1) {
		if (*state == -1) {
			// Process initialization state
			init_tlc();
			(*state)++;
			return;
		}
	//	traffic_states {RR0, GR, YR, RR1, RG, RY};
		if (timerFlag != 1) {
			switch (currentState) {
				case RR0:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
					alt_alarm_start(&tlc_timer, timeout[0], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
				case GR:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x21);
					alt_alarm_start(&tlc_timer, timeout[1], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
				case YR:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x22);
					alt_alarm_start(&tlc_timer, timeout[2], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
				case RR1:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
					alt_alarm_start(&tlc_timer, timeout[3], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
				case RG:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0xc);
					alt_alarm_start(&tlc_timer, timeout[4], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
				case RY:
					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x14);
					alt_alarm_start(&tlc_timer, timeout[5], tlc_timer_isr, 0);
					timerFlag = 1;
					break;
			}
		}
		// If the timeout has occured
		/*
			// Increase state number (within bounds)
			// Restart timer with new timeout value
		*/
	}
}


/* DESCRIPTION: Handles the traffic light timer interrupt
 * PARAMETER:   context - opaque reference to user data
 * RETURNS:     Number of 'ticks' until the next timer interrupt. A return value
 *              of zero stops the timer.
 */
alt_u32 tlc_timer_isr()
{
	switch (currentState) {
		case RR0:
			currentState = GR;
			break;
		case GR:
			currentState = YR;
			break;
		case YR:
			currentState = RR1;
			break;
		case RR1:
			currentState = RG;
			break;
		case RG:
			currentState = RY;
			break;
		case RY:
			currentState = RR0;
			break;
	}

	timerFlag = 0;

//	volatile int* trigger = (volatile int*)context;
//	*trigger = 1;
	return 0;
}


/* DESCRIPTION: Initialize the interrupts for all buttons
 * PARAMETER:   none
 * RETURNS:     none
 */
void init_buttons_pio(int buttons)
{
	void* context_going_to_be_passed = (void*) &buttons; // cast before passing to ISR
	// Initialize NS/EW pedestrian button
	// Reset the edge capture register
	// clears the edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS_BASE, 0);
	// enable interrupts for all buttons
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
	// register the ISR
	alt_irq_register(BUTTONS_IRQ, context_going_to_be_passed, NSEW_ped_isr);

}


/* DESCRIPTION: Pedestrian traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
void pedestrian_tlc(int* state)
{
	if (mode == 2 || mode == 4) {
		if (*state == -1) {
			// Process initialization state
			init_tlc();
			(*state)++;
			return;
		}

		// Same as simple TLC
		// with additional states / signals for Pedestrian crossings
		// traffic_states {RR0, GR, YR, RR1, RG, RY};
		if (mode == 2){
			if (timerFlag != 1) {
				switch (currentState) {
					case RR0:
						IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
						alt_alarm_start(&tlc_timer, timeout[0], tlc_timer_isr, 0);
						timerFlag = 1;
//						if (pedestrianNS == 1) {
//							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x64);
//						}
						break;
					case GR:
						if (pedestrianNS == 1) {
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x61);
							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
							pedestrianNS = 0;
							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
						} else {
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x21);
						}
						alt_alarm_start(&tlc_timer, timeout[1], tlc_timer_isr, 0);
						timerFlag = 1;
						break;
					case YR:
						IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x22);
						alt_alarm_start(&tlc_timer, timeout[2], tlc_timer_isr, 0);
						timerFlag = 1;
//						if (pedestrianNS == 1) {
//							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
//							pedestrianNS = 0;
//							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
//						}
						break;
					case RR1:
						IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
						alt_alarm_start(&tlc_timer, timeout[3], tlc_timer_isr, 0);
						timerFlag = 1;
//						if (pedestrianEW == 1) {
//							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x84);
//						}
						break;
					case RG:
						if (pedestrianEW == 1) {
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x8c);
							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
							pedestrianEW = 0;
							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
						} else {
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0xc);
						}
						alt_alarm_start(&tlc_timer, timeout[4], tlc_timer_isr, 0);
						timerFlag = 1;
						break;
					case RY:
						IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x14);
						alt_alarm_start(&tlc_timer, timeout[5], tlc_timer_isr, 0);
						timerFlag = 1;
//						if (pedestrianEW == 1) {
//							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
//							pedestrianEW = 0;
//							IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
//						}
						break;
				}
			}
		}
		else if (mode == 4){
			if (timerFlag != 1) {
				switch (currentState) {
					case RR0:
						if (configSwitch == 1){
							if (IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE) == 131076){
								timeout_data_handler();
							}
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
							alt_alarm_start(&tlc_timer, timearray[0], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x64);
							}
						}
						else{
							if (IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE) == 131076){
								timeout_data_handler();
							}
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
							alt_alarm_start(&tlc_timer, timeout[0], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x64);
							}
						}
						break;
					case GR:
						if (configSwitch == 1){
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x21);
							alt_alarm_start(&tlc_timer, timearray[1], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x61);
							}
						}
						else{
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x21);
							alt_alarm_start(&tlc_timer, timeout[1], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x61);
							}
						}
						break;
					case YR:
						if (configSwitch == 1){
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x22);
							alt_alarm_start(&tlc_timer, timearray[2], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
								pedestrianNS = 0;
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
							}
						}
						else{
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x22);
							alt_alarm_start(&tlc_timer, timeout[2], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianNS == 1) {
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
								pedestrianNS = 0;
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
							}
						}
						break;
					case RR1:
						if (configSwitch == 1){
							if (IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE) == 131076){
								timeout_data_handler();
							}
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
							alt_alarm_start(&tlc_timer, timearray[3], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x84);
							}
						}
						else{
							if (IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE) == 131076){
								timeout_data_handler();
							}
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x24);
							alt_alarm_start(&tlc_timer, timeout[3], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x84);
							}
						}
						break;
					case RG:
						if (configSwitch == 1){
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0xc);
							alt_alarm_start(&tlc_timer, timearray[4], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x8c);
							}
						}
						else{
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0xc);
							alt_alarm_start(&tlc_timer, timeout[4], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x8c);
							}
						}
						break;
					case RY:
						if (configSwitch == 1){
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x14);
							alt_alarm_start(&tlc_timer, timearray[5], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
								pedestrianEW = 0;
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
							}
						}
						else{
							IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x14);
							alt_alarm_start(&tlc_timer, timeout[5], tlc_timer_isr, 0);
							timerFlag = 1;
							if (pedestrianEW == 1) {
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x0);
								pedestrianEW = 0;
								IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS_BASE, 0x3);
							}
						}
						break;
				}
			}
		}
	}

}


/* DESCRIPTION: Handles the NSEW pedestrian button interrupt
 * PARAMETER:   context - opaque reference to user data
 *              id - hardware interrupt number for the device
 * RETURNS:     none
 */
void NSEW_ped_isr(void* context, alt_u32 id)
{
	// NOTE:
	// Cast context to volatile to avoid unwanted compiler optimization.
	// Store the value in the Button's edge capture register in *context
	int* temp = (int*) context; // need to cast the context first before using it
	(*temp) = IORD_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS_BASE);
	// clear the edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS_BASE, 0);	// reset the button flag
	if (*temp == 1) {
		pedestrianNS = 1;
	} else if (*temp == 2) {
		pedestrianEW = 1;
	} else if (*temp == 3) {
		pedestrianEW = 1;
		pedestrianNS = 1;
	}
}


/* DESCRIPTION: Configurable traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
/*
If there is new configuration data... Load it.
Else run pedestrian_tlc();
*/
void configurable_tlc(int* state)
{
	if (*state == -1) {
		// Process initialization state
		init_tlc();
		(*state)++;
		return;
	}

	if (mode == 4) {
		pedestrian_tlc(&proc_state[1]);
	}
	else if (mode == 131076) {
		if (currentState == RR0 || currentState == RR1) {
			timeout_data_handler();
		}
		pedestrian_tlc(&proc_state[1]);
	}

}


/* DESCRIPTION: Implements the state machine of the traffic light controller in
 *              the ***configuration*** phase
 * PARAMETER:   tl_state - state of the traffic light
 * RETURNS:     Returns the state of the configuration phase
 */
/*
Puts the TLC in a 'safe' state... then begins update
*/
int config_tlc(int* tl_state)
{
	// State of configuration
	static int state = 0;

	if (*tl_state == -1) {
		// Process initialization state
		state = 0;
		return 0;
	}

	return state;
}


/* DESCRIPTION: Parses the configuration string and updates the timeouts
 * PARAMETER:   none
 * RETURNS:     none
 */
/*
 buffer_timeout() must be used 'for atomic transfer to the main timeout buffer'
*/
void timeout_data_handler(void)
{
	char uartinput = '2';
	int intvalue;
	int thousands, hundreds, tens, ones, placecount = 0;
	int arrayvalue = 0;
	int i;
	int j;
	error = 0;
	uartinput = fgetc(uart);
	configSwitch = 0;

	while (uartinput != ',') {
		intvalue = uartinput - '0';
		if (placecount == 0){
			thousands = intvalue;
			arrayvalue = thousands;
			placecount++;
		}
		else if (placecount == 1){
			hundreds = intvalue;
			arrayvalue = thousands * 10 + hundreds;
			placecount++;
		}
		else if (placecount == 2){
			tens = intvalue;
			arrayvalue = thousands * 100 + 10 * hundreds + tens;
			placecount++;
		}
		else if (placecount == 3){
			ones = intvalue;
			arrayvalue = thousands * 1000 + 100 * hundreds + 10 * tens + ones;
			placecount++;
		}
		else if (placecount == 4){
			error = 1;
		}
		if (uartinput == '\n'){
			endString = 1;
		}
		printf ("ErrorValue: %d \n", error);
		printf ("End String Reach?: %d \n", endString);
		uartinput = fgetc(uart);
	}
	if (error == 0){
		timearray[arrayCount] = arrayvalue;
		arrayCount = arrayCount + 1;
	}
	//printf("%d\n", timearray[0]);
	for(i = 0; i < 6; i=i+1){
        printf("%d \n", timearray[i]);
	}
	if (arrayCount != 6){
		timeout_data_handler();
	}
	for(i = 0; i < 6; i=i+1){
		if (timearray[i] == 0){
			error = 1;
		}
	}
	if (error == 1){
		printf ("Error Reached");
		for(i = 0; i < 6; i=i+1){
			timearray[i] = 0;
		}
		arrayCount = 0;
	}
	/*
	if (endString == 1){
		for(i = 0; i < 6; i=i+1){
			if (timearray[i] == 0){
				for(j = 0; j < 6; j=j+1){
					timearray[j] = 0;
				}
			}
		}
	}*/
	while (IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE) == 131076){
	}
	configSwitch = 1;
	mode = 4;
	pedestrian_tlc(&proc_state[1]);

}


/* DESCRIPTION: Stores the new timeout values in a secondary buffer for atomic

 *              transfer to the main timeout buffer at a later stage
 * PARAMETER:   value - value to store in the buffer
 * RETURNS:     none
 */
void buffer_timeout(unsigned int value)
{

}


/* DESCRIPTION: Implements the update operation of timeout values as a critical
 *              section by ensuring that timeouts are fully received before
 *              allowing the update
 * PARAMETER:   none
 * RETURNS:     1 if update is completed; 0 otherwise
 */
int update_timeout(void)
{

}

/* DESCRIPTION: Handles the red light camera timer interrupt
 * PARAMETER:   context - opaque reference to user data
 * RETURNS:     Number of 'ticks' until the next timer interrupt. A return value
 *              of zero stops the timer.
 */
alt_u32 camera_timer_isr(void* context)
{
	volatile int* trigger = (volatile int*)context;
	*trigger = 1;
	return 0;
}

/* DESCRIPTION: Camera traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
 /*
 Same functionality as configurable_tlc
 But also handles Red-light camera
 */
void camera_tlc(int* state)
{
	if (*state == -1) {
		configurable_tlc(state);
		return;
	}

}


/* DESCRIPTION: Simulates the entry and exit of vehicles at the intersection
 * PARAMETER:   none
 * RETURNS:     none
 */
void handle_vehicle_button(void)
{

}

// set vehicle_detected to 'no vehicle' state
void clear_vehicle_detected(void)
{
}
// set vehicle_detected to 'checking' state
void vehicle_checked(void)
{
}
// return true or false if a vehicle has been detected
int is_vehicle_detected(void)
{
}
// return true or false if the vehicle has left the intersection yet
int is_vehicle_left(void)
{
}

//void switches_interrupts_function(void* context, alt_u32 id){
//	int* temp = (int*) context; // need to cast the context first before using it
//	//int switches_value;
//	(*temp) = IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);
//		// clear the edge capture register
//	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(SWITCHES_BASE, 0);	// reset the button flag
//	printf("Switches: %i\n", *temp);
//
//		//switches_value =  IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);
//		if (*temp == 1){
//			printf("Mode 1\n");
//		}
//		else if (*temp == 2){
//			printf("Mode 2\n");
//		}
//		else if (*temp == 3){
//			printf("Mode 3\n");
//		}
//		else if (*temp == 4){
//			printf("Mode 4\n");
//		}
//}



int main(void)
{
	lcd = fopen(LCD_NAME, "w");
	uart = fopen(UART_NAME,"r");

	if (uart == NULL) {
		printf("uart opened incorrectly!\n");
	}

	int buttons = 0;			// status of mode button
	lcd_set_mode(0);		// initialize lcd

	init_buttons_pio(buttons);			// initialize buttons

//	int switchValue = 1;
//	void* context_going_to_be_passed = (void*) &switchValue;
//	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(SWITCHES_BASE, 0xF);
//	alt_irq_register(SWITCHES_IRQ,context_going_to_be_passed,
//				switches_interrupts_function);

//	currentState = RR0;
	while (1) {
		// Button detection & debouncing
		mode = IORD_ALTERA_AVALON_PIO_DATA(SWITCHES_BASE);

		// if Mode button pushed:
			// Set current TLC state to -1
			// handle_mode_button to change state & display
		// if Car button pushed...
			// handle_vehicle_button

		// Execute the correct TLC

			switch (mode) {
				case 0:
//					currentState = RY;
//					IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE, 0x0);
					break;
				case 1:
					simple_tlc(&proc_state[0]);
					break;
				case 2:
					pedestrian_tlc(&proc_state[1]);
					break;
				case 4:
					configurable_tlc(&proc_state[2]);
					break;
				case 8:
					camera_tlc(&proc_state[3]);
					break;
				case 131076:						// mode 3 and receive new timeout values (SW17 is high)
					configurable_tlc(&proc_state[2]);
					break;
			}

		// Update Displays
    	if (lcd != NULL){
    		if (previousMode != mode){
    			lcd_set_mode(mode);
    		}
    	}


	}

	fclose(lcd);
	return 1;
}
